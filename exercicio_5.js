function divisivelPor3(number){
    return number % 3 === 0
}

function divisivelPor5(number){
    return number % 5 === 0
}

function buzzfizz(number){
    if (divisivelPor3(number) && divisivelPor5(number)) {
        console.log('buzzfizz')
    
    } else if(divisivelPor3(number)) {
        console.log('buzz')
    
    } else if(divisivelPor5(number)) {
        console.log('fizz')
    }
}

buzzfizz(15)